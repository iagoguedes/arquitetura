package com.arquitetura.back.service;

import com.arquitetura.back.entity.Pessoa;
import com.arquitetura.back.repository.PessoaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Integer.parseInt;

@Service
public class PessoaService {

    @Autowired
    PessoaRepository repository;

    public List<Pessoa> findAll(){
        return repository.findAll();
    }

    public Pessoa findById(long id){
        return repository.findById(id).orElse(null);
    }

    public String validate(Pessoa pessoa) throws ParseException {
        String pessoaComEmailExistente = repository.exstingEmail(pessoa.getEmail());

        if(pessoaComEmailExistente != null) {
            return "Email existente";
        }

        Date nascimento = pessoa.getNascimento();

        DateFormat dateFormat = new SimpleDateFormat("yyyy");
        int anoNascimento = parseInt(dateFormat.format(nascimento));
        int anoAtual = parseInt(dateFormat.format(new Date()));


        if((anoAtual - anoNascimento) < 18 ){
            return "Menor de 18 anos não é permitido";
        }

        return null;
    }

    public Pessoa save (Pessoa pessoa) {
        return repository.save(pessoa);
    }

    public boolean verificarPessoaExistente (Long id){
        if(id == 0 ){
            return false;
        }

        Optional<Pessoa> pessoaExistente = repository.findById(id);
        if(pessoaExistente == null) {
            return false;
        }

        return true;
    }

    public Pessoa update(Pessoa pessoa){
        Pessoa pessoaBanco = this.findById(pessoa.getId());
        if(pessoaBanco != null){
            return repository.save(pessoa);
        } else {
            return null;
        }
    }

    public boolean delete(long id){
        Pessoa pessoa = this.findById(id);
        if(pessoa != null){
            repository.delete(pessoa);
            return true;
        } else {
            return false;
        }
    }

}
