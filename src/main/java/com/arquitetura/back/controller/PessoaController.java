package com.arquitetura.back.controller;

import com.arquitetura.back.entity.Pessoa;
import com.arquitetura.back.service.PessoaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    @Autowired
    PessoaService service;

    @GetMapping
    public ResponseEntity<?> findAll(){
        List<Pessoa> lista = service.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById( @PathVariable long id){
        Pessoa pessoa = service.findById(id);

        if(pessoa != null){
            return new ResponseEntity<>(pessoa, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Nao existe pessoa com esse id", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<?> save( @RequestBody Pessoa pessoa ) throws ParseException {
        String errors = service.validate(pessoa);
        if(errors != null) {
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }
        Pessoa p = service.save((pessoa));
        return new ResponseEntity<>(p, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> update( @RequestBody Pessoa pessoa ){
        if(!service.verificarPessoaExistente(pessoa.getId())){
            return new ResponseEntity<>("Nao existe pessoa com esse id", HttpStatus.NOT_FOUND);
        }

        Pessoa p = service.update(pessoa);
        return new ResponseEntity<>(p, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete( @PathVariable long id){
        if(service.delete(id)){
            return new ResponseEntity<>(null, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
